import { PrismicRichText } from "@prismicio/react"
import { GatsbyImage, IGatsbyImageData } from "gatsby-plugin-image"
import React, { Key } from "react"

import {
  TeamPageDocumentDataBodyTeamSectionSlice,
  TeamProfileDocument,
} from "../../types.generated"
import Collapsible from "./Collapsible"
import ProseBlock from "./ProseBlock"

type TeamProfilePictureProps = {
  gatsbyImageData?: IGatsbyImageData
  alt?: string
}

const TeamProfilePicture = ({
  gatsbyImageData,
  alt,
}: TeamProfilePictureProps) => (
  <div className="rounded-full max-w-xs max-h-xs">
    {gatsbyImageData && (
      <GatsbyImage
        className="rounded-full"
        image={gatsbyImageData}
        alt={alt || ""}
      />
    )}
  </div>
)

type TeamProfileProps = {
  key?: Key
  data: TeamProfileDocument
}

const TeamProfile = ({ data, key }: TeamProfileProps) => (
  <div
    key={key}
    className="flex flex-col py-4 md:flex-row md:items-start items-center"
  >
    <div className="my-4 me-8 md:basis-1/3">
      <TeamProfilePicture
        gatsbyImageData={data.profile_picture?.gatsbyImageData}
        alt={data.profile_picture?.alt}
      />
    </div>
    <div className="my-4 md:basis-2/3">
      <h2 className="text-2xl font-semibold">{data.name}</h2>
      <h3 className="text-xl font-light mb-4">{data.title_info}</h3>
      <ProseBlock>
        <PrismicRichText field={data.bio.richText} />
      </ProseBlock>
    </div>
  </div>
)

type TeamSectionSliceProps = {
  slice: TeamPageDocumentDataBodyTeamSectionSlice
  slice_type: string
}

const TeamSection = ({ slice }: TeamSectionSliceProps) => {
  if (slice.items.length < 1) {
    return <></>
  }

  return [
    <Collapsible
      title={slice.primary?.title?.text || "Our Team"}
      subtitle={""}
      open={true}
      className="mt-4"
    >
      <div className="flex flex-col divide-y md:divide-solid">
        {slice.items.map(
          (
            {
              team_profile,
            }: { team_profile?: { document?: { data?: TeamProfileDocument } } },
            index: number
          ) => (
            <TeamProfile data={team_profile?.document?.data} key={index} />
          )
        )}
      </div>
    </Collapsible>,
  ]
}

export default TeamSection
