import { PrismicRichText } from "@prismicio/react"
import { GatsbyImage } from "gatsby-plugin-image"
import React from "react"

import { BlogPostDocument } from "../../types.generated"
import { dateToUTC } from "../utils/prismic"
import ImageWithCaption from "./ImageWithCaption"
import ProseBlock from "./ProseBlock"

const Subtitle = ({ page }: { page: any }) => {
  if (page.subtitle?.text) {
    return <h2 className="text-2xl mb-4">{page.subtitle.text}</h2>
  }
}

const BlogPostContent = ({ blogPost }: BlogPostDocument) => {
  const pageData = blogPost.data
  if (!pageData || Object.keys(pageData).length === 0) {
    return null
  }
  const date = dateToUTC(pageData.date)
  const authors = pageData.authors
    .map(
      (element: { team_profile: { document: any } }) =>
        element.team_profile?.document
    )
    .filter((author: any) => author?.data?.name !== undefined)

  const customComponents = {
    image: ({ node }: { node: any }) => <ImageWithCaption node={node} />,
  }

  return (
    <main className="container mx-auto max-w-2xl px-6 py-10">
      <header className="pb-8 border-b border-ul-bright-blue mb-8">
        {pageData?.cover_image?.gatsbyImageData && (
          <div className="mb-10">
            <GatsbyImage
              className="w-full"
              image={pageData.cover_image.gatsbyImageData}
              alt={pageData.cover_image.alt || ""}
            />
          </div>
        )}
        <h1 className="text-5xl mb-6">{pageData?.title?.text}</h1>
        <Subtitle page={pageData} />
        <div className="flex flex-wrap">
          {authors?.map((author: any, index: React.Key | null | undefined) => (
            <div
              key={index}
              className={`flex flex-row ${authors.length === 1 ? "" : "md:w-1/2 "} w-full p-2 ${authors.length > 1 ? "mt-1" : ""}`}
            >
              <div
                className="rounded-full overflow-hidden w-20 h-20 me-4"
                style={{
                  flexShrink: 0,
                }}
              >
                <GatsbyImage
                  className="w-full h-full"
                  image={author?.data.profile_picture.gatsbyImageData}
                  alt={author?.data.profile_picture.alt || ""}
                  imgStyle={{ objectFit: "cover" }}
                />
              </div>
              <div className="flex grow flex-col">
                <span className="text-lg">{author?.data.name}</span>
                <span className="font-light">{author?.data.title_info}</span>
                {authors.length === 1 && (
                  <span className="font-light">
                    {date?.toDateString() || ""}
                  </span>
                )}
              </div>
            </div>
          ))}
        </div>

        {authors.length > 1 && (
          <div className="mt-6 font-light">{date?.toDateString() || ""}</div>
        )}
      </header>
      <ProseBlock>
        <PrismicRichText
          field={pageData.content.richText}
          components={customComponents}
        />
      </ProseBlock>
    </main>
  )
}

export default BlogPostContent
