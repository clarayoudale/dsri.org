import type { GatsbyConfig } from "gatsby"

require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

const config: GatsbyConfig = {
  siteMetadata: {
    title: "Digital Safety Research Institute",
    description:
      "As part of UL Research Institutes, DSRI researches methods to increase public safety in the digital ecosystem.",
    siteUrl: "https://dsri.org",
  },
  // More easily incorporate content into your pages through automatic TypeScript type generation and better GraphQL IntelliSense.
  // If you use VSCode you can also use the GraphQL plugin
  // Learn more at: https://gatsby.dev/graphql-typegen
  graphqlTypegen: true,
  plugins: [
    "gatsby-plugin-image",
    {
      resolve: "gatsby-plugin-manifest",
      /**
       * @type {import("gatsby-plugin-manifest").PluginOptions}
       */
      options: {
        name: "Digital Safety Research Institute",
        short_name: "DSRI",
        start_url: "/",
        icon: "src/images/ulresearchinst_logo_horz_blue_rgb-150x150.png",
      },
    },
    "gatsby-plugin-postcss",
    {
      resolve: "gatsby-plugin-react-svg",
      options: {
        rule: {
          include: /svg/,
        },
      },
    },
    {
      resolve: "gatsby-plugin-robots-txt",
      options: {
        policy: [{ userAgent: "*", allow: "/" }],
      },
    },
    "gatsby-plugin-sharp",
    "gatsby-plugin-sitemap",
    {
      resolve: "gatsby-source-prismic",
      /**
       * @type {import("gatsby-source-prismic").PluginOptions}
       */
      options: {
        repositoryName: process.env.GATSBY_PRISMIC_REPO_NAME,
        accessToken: process.env.PRISMIC_ACCESS_TOKEN,
        customTypesApiToken: process.env.PRISMIC_CUSTOM_TYPES_API_TOKEN,
        previews: true,
        linkResolver: require("./src/utils/LinkResolver").default,
      },
    },
    {
      resolve: "gatsby-plugin-prismic-previews",
      options: {
        repositoryName: process.env.GATSBY_PRISMIC_REPO_NAME,
        accessToken: process.env.PRISMIC_ACCESS_TOKEN,
        linkResolver: require("./src/utils/LinkResolver").default,
      },
    },
  ],
}

export default config
