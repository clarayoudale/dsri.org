import React, { ReactElement } from "react"

type ButtonProps = {
  text: string
  url: string
  hoverLinks?: {
    text: string
    url: string
  }
}

const Button = ({ text, url }: ButtonProps): ReactElement => (
  <a href={url}>
    <button className="bg-ul-bright-blue text-white font-semibold py-2 px-4 rounded-full hover:bg-black transition duration-300">
      {text}
    </button>
  </a>
)

export default Button
