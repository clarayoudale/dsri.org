# dsri.org website

<!-- BADGIE TIME -->

[![pipeline status](https://img.shields.io/gitlab/pipeline-status/dsri-org/dsri.org?branch=main)](https://gitlab.com/dsri-org/dsri.org/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

This is the source of the DSRI home page at [dsri.org](https://dsri.org).

Visit the draft site at
<https://dsri-org-ul-dsri-sites-418571e4cccf8fbead86a550c336802b2c8eefaa.gitlab.io/>.

See [CONTRIBUTING.md](CONTRIBUTING.md) for development information
