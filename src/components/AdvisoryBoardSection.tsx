import { PrismicRichText } from "@prismicio/react"
import React from "react"

import {
  AdvisoryBoardDocumentDataBodyAdvisoryBoardMemberSlice,
  AdvisoryBoardMemberDocument,
} from "../../types.generated"
import Collapsible from "./Collapsible"

type AdvisoryBoardSectionSliceProps = {
  slice: AdvisoryBoardDocumentDataBodyAdvisoryBoardMemberSlice
  slice_type: string
}

const AdvisoryBoardSection = ({ slice }: AdvisoryBoardSectionSliceProps) => {
  if (slice.items.length < 1) {
    return <></>
  }

  return (
    <Collapsible
      title={"Advisory Board"}
      subtitle={slice.primary.intro.richText[0]?.text}
      open={true}
      className="mt-4"
    >
      <div className="flex flex-col divide-y md:divide-solid mt-1">
        {slice.items.map(
          (
            {
              advisory_board_member,
            }: {
              advisory_board_member?: {
                document?: {
                  data?: AdvisoryBoardMemberDocument
                }
              }
            },
            index: number
          ) => {
            const data = advisory_board_member?.document?.data
            if (!data) return <></>
            return (
              <div
                key={index}
                className="flex flex-col pt-3 items-start content-start"
              >
                <h5 className="font-medium hover:text-ul-bright-blue underline transition-all">
                  <a href={data.name_link.url} target="_blank">
                    {data.name}
                  </a>
                </h5>
                <div className="container prose prose-p:mb-3 leading-8 mx-auto max-w-5xl font-light text-black">
                  <PrismicRichText field={data.bio.richText} />
                </div>
              </div>
            )
          }
        )}
      </div>
    </Collapsible>
  )
}

export default AdvisoryBoardSection
