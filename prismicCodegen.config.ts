import type { Config } from "prismic-ts-codegen"

require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

const config: Config = {
  output: "./types.generated.ts",
  repositoryName: process.env.GATSBY_PRISMIC_REPO_NAME,
  customTypesAPIToken: process.env.PRISMIC_CUSTOM_TYPES_API_TOKEN,
  models: {
    fetchFromRepository: true,
  },
}

export default config
