import { StaticImage } from "gatsby-plugin-image"
import React, { ReactElement } from "react"

const TimelineGraphic = (): ReactElement => (
  <>
    <div className="hidden md:block">
      <StaticImage
        src="../images/DSRI_Timeline_horizontal.png"
        alt="timeline"
      />
    </div>
    <div className="md:hidden">
      <StaticImage src="../images/DSRI_Timeline_vertical.png" alt="timeline" />
    </div>
  </>
)

export default TimelineGraphic
