import { graphql, PageProps } from "gatsby"
import React from "react"

import OpenPosition from "../../components/OpenPosition"
import Page from "../../components/Page"
import ProseBlock from "../../components/ProseBlock"
import { SEO } from "../../components/SEO"

export const Head = (props: PageProps) => (
  <SEO
    title="Careers"
    description="Check out full-time and contract jobs at the Digital Safety Research Institute and other opportunities to join UL Research Institutes."
    pathname={props.location.pathname}
  />
)

// TODO: Handle when there are no open positions.
const CareersPage = ({ data }: PageProps<Queries.CareersPageQuery>) => (
  <Page title="Join Our Team">
    <ProseBlock>
      <p>
        Would you like to help create a better digital safety ecosystem? Do you
        have ideas for how to help fix it? Check out full-time and contract jobs
        at the Digital Safety Research Institute and other opportunities to join
        UL Research Institutes.
      </p>
    </ProseBlock>
    <div className="flex flex-col gap-4 divide-y divide-solid">
      {data.allPrismicOpenPosition.nodes.length == 0 && (
        <div className="mx-auto"></div>
      )}
      {data.allPrismicOpenPosition.nodes.length > 0 &&
        data.allPrismicOpenPosition.nodes.map((open_position, index) => (
          <OpenPosition
            key={index}
            title={open_position.data.title.text || ""}
            url={open_position.data.application_link?.url || ""}
            description={open_position.data.description.richText}
          />
        ))}
    </div>
  </Page>
)

export default CareersPage

export const CareersPageQuery = graphql`
  query CareersPage {
    allPrismicOpenPosition(sort: { data: { weight: DESC } }) {
      nodes {
        data {
          application_link {
            url
          }
          description {
            richText
          }
          title {
            text
          }
        }
      }
    }
  }
`
