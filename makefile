.PHONY: all
all:

.PHONY: build
build:
	npx gatsby build

.PHONY: clean
clean:
	npx gatsby clean

.PHONY: develop
develop:
	npx gatsby develop

.PHONY: gen-prismic-types
gen-prismic-types:
	npx prismic-ts-codegen
	npx prettier -w types.generated.ts
