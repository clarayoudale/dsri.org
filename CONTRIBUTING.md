# Development

### Prerequisites

A recent version of [Node.js](https://nodejs.org/en) is required.

Content is provided by a [Prismic](https://prismic.io/) backend, and credentials
are required to access this content at build time.

### Environment

Add the required `.env.development` file:

```shell
GATSBY_PRISMIC_REPO_NAME=dsri
PRISMIC_API_TOKEN=<SHARED_SECRET>
PRISMIC_CUSTOM_TYPES_API_TOKEN=<SHARED_SECRET>
```

API Token is for access to access

Custom Types API Token is for access to custom types (templates for documents)

### Execution

Install required dependencies:

```bash
npm install
```

Run the Gatsby development server:

```bash
make develop
```

Build the production site in the `public/` directory:

```bash
make build
```

To update Prismic types (in `types.generated.ts`) when Prismic Custom Types are added / updated in the UI:

```bash
make gen-prismic types
```

If you get the error `"repositoryName" is required` when generating types, your `.env.development` may be not be recognized correctly.

Set your `NODE_ENV` env variable to development:

```shell
export NODE_ENV=development
```

### Dynamic Content Flow

The dynamic content flow is summarized in the following steps:

- Custom types are constructed using Prismic's online interface. These define the structure of documents content, such as a title, paragraph, images, links etc.

- Documents are created utilizing custom types to define data within the type structure. This is where the actual content is populated into the type structure.

  - CAUTION: Once Documents are published (to the production DSRI Prismic namespace), any Gatsby queries will fetch those document changes.

  - TODO: dsri-dev staging environment to preview Document / Types

- Gatsby fetches documents from Prismic using GraphQL queries and the `gatsby-source-prismic` plugin, authenticated using `PRISMIC_API_TOKEN` defined above.

  GraphQL queries are created using the `graphql` tag

  ```tsx
  export const AboutPageQuery = graphql`
    query myQuery {
      somePageQuery () {
        ...
      }
    }`;
  ```

  The fetched content is then made available to components as props from the `Queries` namespace:

  ```tsx
  const AboutPage = ({ data }: PageProps<Queries.AboutPageQuery>) => {
    const queryData = data.somePageQuery;
    // Do stuff
  };
  ```

### Formatting

Pre-commit hooks take care of various file formatting throughout the project. Setup pre-commit with:

```bash
pre-commit install
```

TypeScript + others are governed by `prettier` which references the
config in `.prettierrc`, which is also triggered by pre-commit

If you'd like to format TypeScript manually, you can use the `prettier` VSCode extension, and will respect the local `.prettierrc`.
